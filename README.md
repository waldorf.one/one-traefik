# One Traefik

Traefik container.


Generate the password
```
cat /dev/urandom | tr -dc a-zA-Z0-9 | head -c25; echo
```

Hash the password
```
htpasswd -nb admin password | sed -e s/\\$/\\$\\$/g
```
